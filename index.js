import express from 'express';
import dotenv from 'dotenv';

dotenv.config();

const app = express();
const PORT = 5000;

app.use('/', (req, res) => {
    res.send('Hello');
});

app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});
